﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TD3
{
    class Parc
    {
        /// <summary>
        /// chemin d'accès au fichier texte
        /// </summary>
        private string nomFic;
        /// <summary>
        /// Liste des camions mis en vente
        /// </summary>
        private List<Camion> L;

        public Parc(string fichier)
        {
           StreamReader file = new StreamReader(fichier);
           string str;

           L = new List<Camion>();
           while ((str = file.ReadLine()) != null)
           {
               Camion c = new Camion(str);
            L.Add(c);
           }
           file.Close();
        }

        /// <summary>
        /// Affichage des camions en vente
        /// </summary>
        public void Affiche()
        {
            foreach (Camion c in L)
            {
                c.Affiche();
            }
        }
    }
}
