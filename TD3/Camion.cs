﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TD3
{
    class Camion
    {
        /// <summary>
        /// Marque
        /// </summary>
        private string marque;
        /// <summary>
        /// Désignation commerciale
        /// </summary>
        private string type;
        /// <summary>
        /// Année de fabrication
        /// </summary>
        private int annee;
        /// <summary>
        /// Puissance moteur (ch vapeur)
        /// </summary>
        private int puissance;
        /// <summary>
        /// Prix de vente
        /// </summary>
        private double prixVente;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="ligne">ligne issue du fichier texte</param>
        public Camion(string ligne)
        {
            string[] tab = ligne.Split('_');
            marque = tab[0];
            type = tab[1];
            annee = int.Parse(tab[2]);
            puissance = int.Parse(tab[3]);
            prixVente = double.Parse(tab[4]);
        }

        /// <summary>
        /// Affichage de la amrque et du type
        /// </summary>
        public void Affiche()
        {
            Console.WriteLine("{0} {1}", marque, type);
        }
    }
}
